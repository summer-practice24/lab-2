#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {
    double a, b, epsilon;
    int m;
    cout << "Enter the values of a, b, and epsilon: ";
    cin >> a >> b >> epsilon;
    m = 10;
    cout << "+-------|------------|------------|---------|----------------+" << endl;
    cout << "|   x   |            |    ~       | accuracy|    number      |" << endl;
    cout << "|       |    f(x)    |   f(x)     |         | of iterations  |" << endl;
    cout << "+-------|------------|------------|---------|----------------+" << endl;

    for (int i = 0; i < m; i++) {
        double x = a + i * (b - a) / (m - 1);
        double function = sqrt(x * x + 1);
        double explicitdecomp = 1.0;
        double term = 1.0;
        int iterations = 0;

        for (int n = 0; fabs(term) > epsilon; n++) {
            explicitdecomp += pow(-1, n) * pow(x, n);
            term = pow(-1, n) * pow(x, n);
            iterations++;
        }

        double accuracy = fabs(function - explicitdecomp);

        cout << "|" << fixed << setw(7) << setprecision(3) << x << "|"
            << fixed << setw(12) << setprecision(6) << function << "|"
            << fixed << setw(12) << setprecision(6) << explicitdecomp << "|"
            << fixed << setw(9) << setprecision(6) << accuracy << "|"
            << fixed << setw(7) << setprecision(6) << iterations << "         |" << endl;
    }

    cout << "+-------|------------|------------|---------|----------------+" << endl;

    return 0;
}